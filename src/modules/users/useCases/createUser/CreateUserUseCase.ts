import { AppError } from "../../../../shared/core/AppError";
import { left, Result, right } from "../../../../shared/core/Result";
import { UseCase } from "../../../../shared/core/UseCase";
import { User } from "../../domain/user";
import { UserEmail } from "../../domain/userEmail";
import { UserName } from "../../domain/userName";
import { UserPassword } from "../../domain/userPassword";
import { IUserRepo } from "../../repos/userRepo";
import { CreateUserDTO } from "./CreateUserDTO";
import { CreateUserErrors } from "./CreateUserErrors";
import { CreateUserResponse } from "./CreateUserResponse";

export class CreateUserUseCase implements UseCase<CreateUserDTO, Promise<CreateUserResponse>> {
    private userRepo: IUserRepo

    constructor(userRepo: IUserRepo){
        this.userRepo = userRepo
    }

    public async execute(request: CreateUserDTO): Promise<CreateUserResponse> {
        const emailOrError = UserEmail.create(request.email)
        const passwordOrError = UserPassword.create({ value: request.password })
        const usernameOrError = UserName.create({ name: request.username })

        const dtoResult = Result.combine([
            emailOrError, passwordOrError, usernameOrError,
        ])

        if (dtoResult.isFailure) {
            return left(Result.fail<void>(dtoResult.error))
        }

        const email: UserEmail = emailOrError.getValue();
        const password: UserPassword = passwordOrError.getValue();
        const username: UserName = usernameOrError.getValue();

        try {
            const userAlreadyExists = await this.userRepo.exists(email)

            if (userAlreadyExists) {
                return left(new CreateUserErrors.EmailAlreadyExistsError(email.value))
            }

            try {
                const alreadyCreatedUserByUserName = await this.userRepo.getUserByUserName(username)
                
                const userNameTaken = !!alreadyCreatedUserByUserName

                if (userNameTaken) {
                    return left(
                        new CreateUserErrors.UsernameTakenError(username.value)
                    )
                }
            } catch (err) {}

            const userOrError = User.create({
                email, password, username
            })

            if (userOrError.isFailure) {
                return left(
                    Result.fail<User>(userOrError.error.toString())
                )
            }

            const user: User = userOrError.getValue()

            await this.userRepo.save(user)

            return right(Result.ok<void>())

        } catch (err) {
            return left(new AppError.UnexpectedError(err))
        }
    }
}